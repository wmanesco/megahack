import { Component } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { UserService } from '../user.service';

@Component({
  selector: 'app-recompensas',
  templateUrl: 'recompensas.page.html',
  styleUrls: ['recompensas.page.scss']
})
export class RecompensasPage {
  public parceiros;
  public pontos = 0;

  constructor(private alertController: AlertController, private toastController: ToastController,
              private userService: UserService) {}

  ngOnInit() {
    this.refreshPontos();

    // this.pontos = 200;
    this.parceiros = [
      {
        img: "./../assets/images/ifood.png",
        nome: "Ifood",
        descricao: "Ganhe um cupom de R$ 10,00 no Ifood",
        valor: 100
      },
      {
        img: "./../assets/images/americanas.png",
        nome: "Americanas",
        descricao: "Ganhe um cashback de R$ 10,00 na americanas",
        valor: 100
      },
      {
        img: "./../assets/images/dotz.jpeg",
        nome: "Dotz",
        descricao: "Resgate 100 pontos Dotz!",
        valor: 100
      }
    ]
  }

  refreshPontos(callback = () => {}){
    this.userService.getTotalPontos((data, err)=> {
      if (data && data.totalPontos) {
        this.pontos = data.totalPontos;
        callback()
      } else {
        callback()
      }
    });
  }

  resgatar(parceiro){
    if (this.pontos < parceiro.valor) {
      this.presentToast("Pontos insuficientes!")
    } else {
      this.presentAlertConfirm("Confirma o resgate no valor de " + parceiro.valor.toString() + " pontos?", () => {
  
      }, () => {
        this.userService.debitaPontos({pontos: parceiro.valor, operacao: parceiro.nome}, (data, err) => {
          if (err){
            this.presentToast("Houve um erro ao resgatar sua recompensa!")
          } else {
            this.pontos -= parceiro.valor;
            this.presentToast("Parabéns! Recompensa resgatada com sucesso!")
          }
        })
      })
    }
  }

  doRefresh(event) {
    this.refreshPontos(() => {
      event.target.complete();
    })
  }


  async presentAlertConfirm(message, cancel, confirm) {
    const alert = await this.alertController.create({
      header: 'Atenção!',
      message: message,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'danger',
          handler: cancel
        }, {
          text: 'Confirmar',
          handler: confirm
        }
      ]
    });

    await alert.present();
  }


  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }


}
