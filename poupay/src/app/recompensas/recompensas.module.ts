import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RecompensasPage } from './recompensas.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { RecompensasPageRoutingModule } from './recompensas-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    RecompensasPageRoutingModule
  ],
  declarations: [RecompensasPage]
})
export class RecompensasPageModule {}
