import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { RecompensasPage } from './recompensas.page';

describe('Tab2Page', () => {
  let component: RecompensasPage;
  let fixture: ComponentFixture<RecompensasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RecompensasPage],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(RecompensasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
