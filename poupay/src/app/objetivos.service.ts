import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
// import 'rxjs/add/operator/map';
// import { map } from 'rxjs/operators';

import { HTTP } from '@ionic-native/http/ngx';

import { Plugins } from '@capacitor/core';
import { AlertController } from '@ionic/angular';

const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})

export class ObjetivosService {

  constructor(public http: HTTP, private alertController: AlertController) {

  }

  async getAll(callback) {
    let headers = {
      "Authorization": "Bearer " + await this.getToken()
    }

      this.http.get('http://poupay.us-east-2.elasticbeanstalk.com/objetivos', {}, headers)
      .then(data => callback(JSON.parse(data.data), null) )
      .catch( error => {
        callback(null, JSON.stringify(error),headers)
      });
    
  }

  async presentAlert(message) {

  }

  async create(objetivo, callback){
    // let headers: Headers = new Headers()
    // headers.append('Authorization', "Bearer " + await this.getToken());

    let headers = {
      "Authorization": "Bearer " + await this.getToken()
    }
        // this.http.post('http://poupay.us-east-2.elasticbeanstalk.com/objetivos', objetivo, {headers: headers}).subscribe({
    //   next: data => callback(data.json(), null),
    //   error: error => {
    //     console.log(error);
        
    //     callback(null, error.json().message ? error.json().message : "Erro desconhecido!")
    //   }
    // });

        
    this.http.post('http://poupay.us-east-2.elasticbeanstalk.com/objetivos', objetivo, headers)
    .then(data => callback(JSON.parse(data.data), null) )
    .catch( error => {
      callback(null, JSON.stringify(error))
    });
    
   }


   async pagar(parcela, callback){
    // let headers: Headers = new Headers()
    // headers.append('Authorization', "Bearer " + await this.getToken());
    let headers = {
      "Authorization": "Bearer " + await this.getToken()
    }
    let body = {
      dataPago: new Date(),
      valorPago: parcela.valor
    }

    // this.http.post('http://poupay.us-east-2.elasticbeanstalk.com/boletos/pagar/' + parcela._id, body, {headers: headers}).subscribe({
    //   next: data => callback(data.json(), null),
    //   error: error => {
    //     console.log(error);
        
    //     callback(null, error.json().message ? error.json().message : "Erro desconhecido!")
    //   }
    // });

    this.http.post('http://poupay.us-east-2.elasticbeanstalk.com/boletos/pagar/' + parcela._id, body, headers)
    .then(data => callback(JSON.parse(data.data), null) )
    .catch( error => {
      callback(null, JSON.stringify(error))
    });
    
    
   }


   async gerarBoleto(parcela, callback){
    // let headers: Headers = new Headers()
    // headers.append('Authorization', "Bearer " + await this.getToken());
    let headers = {
      "Authorization": "Bearer " + await this.getToken()
    }
    // this.http.get('http://poupay.us-east-2.elasticbeanstalk.com/objetivos/boleto/' + parcela._id, {headers: headers}).subscribe({
    //   next: data => callback(data.json(), null),
    //   error: error => {
    //     console.log(error);
        
    //     callback(null, error.json().message ? error.json().message : "Erro desconhecido!")
    //   }
    // });

    this.http.get('http://poupay.us-east-2.elasticbeanstalk.com/objetivos/boleto/', {}, headers)
    .then(data => callback(JSON.parse(data.data), null) )
    .catch( error => {
      callback(null, JSON.stringify(error))
    });

    
   }

   async getToken() {
    const ret = await Storage.get({ key: 'user' });
    const user = JSON.parse(ret.value);

    if (user) {
      return user.token;
    }
   }
}
