import { Component } from '@angular/core';
import { UserService } from '../user.service';


@Component({
  selector: 'app-perfil',
  templateUrl: 'perfil.page.html',
  styleUrls: ['perfil.page.scss']
})
export class PerfilPage {

  public userInfo = {
    nome: "",
    email: ""
  };

  public saldo = 0;

  constructor(private userService: UserService) {}

  ngOnInit(){
    
    this.userService.get((data, err) => {
      this.userInfo = { 
        nome: data.firstName.trim() + ' ' + data.lastName.trim(),
        email: data.email
      }        
    })

    this.refreshSaldo();
    this.userService.getSaldoAtual((data, err) => {
      if(data){
        this.saldo = data.saldo;
      }
    })
  }

  refreshSaldo(callback = () => {}){
    this.userService.getSaldoAtual((data, err) => {
      if(data){
        this.saldo = data.saldo;
        callback()
      } else {
        callback()
      }
    })
  }

  doRefresh(event) {
    this.refreshSaldo(() => {
      event.target.complete();
    })
  }

  floatToMonetary(valor){

    var inteiro = null, decimal = null, c = null, j = null;
    var aux = new Array();

    valor = "" + valor;
    c = valor.indexOf(".",0);
    //encontrou o ponto na string
    if(c > 0){
       //separa as partes em inteiro e decimal
       inteiro = valor.substring(0,c);
       decimal = valor.substring(c+1,valor.length);
    }else{
       inteiro = valor;
    }
       //pega a parte inteiro de 3 em 3 partes
    for (j = inteiro.length, c = 0; j > 0; j-=3, c++){
       aux[c]=inteiro.substring(j-3,j);
    }

    //percorre a string acrescentando os pontos
    inteiro = "";
    for(c = aux.length-1; c >= 0; c--){
       inteiro += aux[c]+'.';
    }
    //retirando o ultimo ponto e finalizando a parte inteiro
    inteiro = inteiro.substring(0,inteiro.length-1);

    if(!decimal)
        decimal = "00";
        
    valor = "R$ "+inteiro+","+decimal.substring(0,2);

    return valor;
 }

}
