import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { FormsModule } from '@angular/forms';
import { ObjetivosPage } from './objetivos.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { ObjetivosPageRoutingModule } from './objetivos-routing.module';
import { ObjetivosCreateModalPage } from './objetivos-create.modal'
import { ObjetivosDetalhesModalPage } from './objetivos-detalhes.modal'

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrMaskerModule } from 'br-mask';
// import { BrMasker4Module } from 'brmasker4';

import { NgCircleProgressModule } from 'ng-circle-progress';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ExploreContainerComponentModule,
    ObjetivosPageRoutingModule,
    BrMaskerModule,
    NgCircleProgressModule.forRoot({
    })
  ],
  declarations: [ObjetivosPage, ObjetivosCreateModalPage, ObjetivosDetalhesModalPage],
  entryComponents: [ObjetivosCreateModalPage, ObjetivosDetalhesModalPage]
})
export class ObjetivosPageModule {}
