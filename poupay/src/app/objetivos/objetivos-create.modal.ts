import { Component, Input, Directive } from '@angular/core';
import { ModalController, ToastController, LoadingController } from '@ionic/angular';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ObjetivosService } from './../objetivos.service'

@Component({
  selector: 'objetivos-create.modal',
  templateUrl: 'objetivos-create.modal.html',
  styleUrls: ['objetivos.page.scss']
})

export class ObjetivosCreateModalPage {

  public form: FormGroup;
  public minDate = new Date().toISOString();
  public selectedCategoria: String;

  constructor(public modalController: ModalController, 
              public formBuilder: FormBuilder,
              public toastController: ToastController,
              public loadingController: LoadingController,
              public objetivosService: ObjetivosService ) {

    this.form = this.formBuilder.group({
      nome: ["", Validators.required ],
      valorTotal: ['', Validators.required],
      valorMensal: ['', Validators.required],
      valorEntrada: [''],
      diaVencimento: ['', Validators.required],
      dataPrimeiraParcela: [new Date(), Validators.required]
    });

  }

  dismiss(){
    this.modalController.dismiss();
  }

  async save() {
    if(this.form.valid && this.selectedCategoria.length > 0){
      const loading = await this.loadingController.create({
        message: 'Gravando...',
        animated: true,
        showBackdrop: true,
        spinner: "bubbles"
      });

      await loading.present();

      let objetivo = Object.assign({}, this.form.value);

      objetivo.categoria = this.selectedCategoria;
      objetivo.diaVencimento = new Date(this.form.value.diaVencimento).getDate();
      objetivo.valorTotal = Number( this.converteDecimal(objetivo.valorTotal) );
      objetivo.valorMensal = Number( this.converteDecimal(objetivo.valorMensal) );
      objetivo.valorEntrada = Number( this.converteDecimal(objetivo.valorEntrada) );
      
      console.log(JSON.stringify(objetivo));
      
      this.objetivosService.create(objetivo, (data, err) => {
        if (err) {
          loading.dismiss()
          this.presentToast(err)
        } else {
            console.log(data);
            
          loading.dismiss()
          this.modalController.dismiss()
        }
      })
      
    } else {
      this.presentToast("Ops, existem dados invalidos!");
    }
    
  }

  selectCategoria(selected) {
    this.selectedCategoria = selected;
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  toNumber(string) {
    return parseFloat(string);
  }

  converteDecimal(valor) {

    if(!valor || valor == "") valor = "0";

    let res = valor.replace(`,`, ``);
    let decimals = res.substring(res.length - 2, res.length)

    return res.substring(0, res.length-2) + `.` + decimals;
  }
}