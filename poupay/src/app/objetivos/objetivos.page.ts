import { Component } from '@angular/core';
import { ModalController, IonRouterOutlet, AlertController } from '@ionic/angular';
import { ObjetivosCreateModalPage } from './objetivos-create.modal';
import { ObjetivosDetalhesModalPage } from './objetivos-detalhes.modal';
import { ObjetivosService } from './../objetivos.service';

@Component({
  selector: 'app-objetivos',
  templateUrl: 'objetivos.page.html',
  styleUrls: ['objetivos.page.scss']
})

export class ObjetivosPage {
  public objetivos;
  constructor(private modalController: ModalController, private objetivosService: ObjetivosService, private alertController: AlertController) {

  }

  ngOnInit() {
    this.refreshData();
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Ops...',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  refreshData(callback = () => {}){
    this.objetivosService.getAll( (data, err ) => {
      this.objetivos = data;
      if(!this.objetivos || this.objetivos.length === 0) {
        callback()
      }

      this.objetivos.forEach( objetivo => {
        let parcelas_pagas = 0; 
        let valorPago = 0;

        objetivo.parcelas.forEach( parcela => {
          if(parcela.valorPago > 0) {
            parcelas_pagas++
            valorPago += parcela.valorPago
          } else {
            parcela.dataPago = null;
          }
          if( new Date(parcela.dataPago).getFullYear() == 1969 ){
            parcela.dataPago = null;
          } //culpa do luizin

        }) 

        objetivo.percentualConcluido = parcelas_pagas / objetivo.parcelas.length;
        objetivo.icon = this.categoriaIcon(objetivo.categoria);
        objetivo.valorPago = valorPago;
        objetivo.parcelasPagas = parcelas_pagas;        
      });

      callback()

    });
  }

  doRefresh(event) {
    this.refreshData(() => {
      event.target.complete();
    })
  }

  async objetivosCreate(){
    const modal = await this.modalController.create({
      component: ObjetivosCreateModalPage
    });

    modal.onDidDismiss().then( saved => {
      if (saved) {
        this.refreshData();
      }
    })

    return await modal.present();
  }

  async detalhesObjetivo(objetivo) {
    // this.presentAlert("aaa")
    const modal = await this.modalController.create({
      component: ObjetivosDetalhesModalPage,
      componentProps: {
        objetivo: objetivo 
      }
    });

    return await modal.present();
  }

  private categoriaIcon(categoria){
    let hash = {};
    hash['SAUDE'] = 'fitness'
    hash['FINANCEIRO'] = 'cash'
    hash['AUTOMOVEL'] = 'car'
    hash['EDUCACAO'] = 'book'
    hash['VIAGENS'] = 'airplane'
    hash['SOCIAL'] = 'people'
    hash['LAZER'] = 'bicycle'
    hash['ESPORTE'] = 'football'
    hash['OUTROS'] = 'ellipsis-horizontal'

    return hash[categoria];
  }

  floatToMonetary(valor){

    var inteiro = null, decimal = null, c = null, j = null;
    var aux = new Array();

    valor = "" + valor;
    c = valor.indexOf(".",0);
    //encontrou o ponto na string
    if(c > 0){
       //separa as partes em inteiro e decimal
       inteiro = valor.substring(0,c);
       decimal = valor.substring(c+1,valor.length);
    }else{
       inteiro = valor;
    }
       //pega a parte inteiro de 3 em 3 partes
    for (j = inteiro.length, c = 0; j > 0; j-=3, c++){
       aux[c]=inteiro.substring(j-3,j);
    }

    //percorre a string acrescentando os pontos
    inteiro = "";
    for(c = aux.length-1; c >= 0; c--){
       inteiro += aux[c]+'.';
    }
    //retirando o ultimo ponto e finalizando a parte inteiro
    inteiro = inteiro.substring(0,inteiro.length-1);

    if(!decimal)
        decimal = "00";
        
    valor = "R$ "+inteiro+","+decimal.substring(0,2);

    return valor;
 }

}
