import { Component, Input, Directive } from '@angular/core';
import { ModalController, ToastController, LoadingController, AlertController, ActionSheetController } from '@ionic/angular';
import { ObjetivosService } from '../objetivos.service'
import { Plugins } from '@capacitor/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { File } from '@ionic-native/file/ngx';
import { Base64 } from '@ionic-native/base64/ngx';

const { Clipboard } = Plugins;

@Component({
  selector: 'objetivos-detalhes.modal',
  templateUrl: 'objetivos-detalhes.modal.html',
  styleUrls: ['objetivos.page.scss']
})

export class ObjetivosDetalhesModalPage {

  @Input() public objetivo;

  constructor(public modalController: ModalController, 
              public toastController: ToastController,
              public loadingController: LoadingController,
              public objetivosService: ObjetivosService,
              private alertController: AlertController,
              public actionSheetController: ActionSheetController,
              public socialSharing: SocialSharing,
              public file: File,
              public base64: Base64 ) {

      // console.log(this.objetivo);
      
  }

  dismiss(){
    this.modalController.dismiss();
  }

  async save() {

  }

  pagar(parcela){
    this.presentAlertConfirm("Confirma o pagamento?", () => {}, () => {
      this.confirma_pagamento(parcela, (callback) => {
        this.objetivosService.gerarBoleto(parcela, (data) => {

          if(!data) {
            callback()
          } else {
            // this.file.writeFile(`${this.file.applicationDirectory}www/assets/`, `${parcela._id}.pdf`, data.pdf)
            this.saveBase64(data.pdf, `${parcela._id}.pdf`, `${this.file.applicationDirectory}www/assets/` )
            .then( fullFileName => {
              this.presentActionSheet(data.linhaDigitavel, `${parcela._id}.pdf`, `${this.file.applicationDirectory}www/assets/`, callback)
            }).catch(err => {
              this.presentToast("Erro ao gerar PDF...")
            })
          }
          
        })
      });
    })
  }

  confirma_pagamento(parcela, geraBoleto){
    this.objetivosService.pagar(parcela, (data, err) => {
      if (err) {
        this.presentToast("Ocorreu um erro ao pagar a parcela...")
      } else {
        geraBoleto( () => {
          parcela.dataPago = new Date();
          parcela.valorPago = parcela.valor;
          this.presentToast("Parcela paga com sucesso...")
        })
      }
    });
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  async presentAlertConfirm(message, cancel, confirm) {
    const alert = await this.alertController.create({
      header: 'Atenção!',
      message: message,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'danger',
          handler: cancel
        }, {
          text: 'Confirmar',
          handler: confirm
        }
      ]
    });

    await alert.present();
  }

  async presentActionSheet(linhaDigitavel, file, path, callback) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Pagar',
      buttons: [{
        text: 'Copiar código de barras',
        icon: 'copy-outline',
        handler: () => {
          Clipboard.write({
            string: linhaDigitavel
          });
          callback()
        }
      }, {
        text: 'Compartilhar PDF',
        icon: 'share-social-outline',
        handler: () => {
          console.log(path + file);
          
          this.socialSharing.share("Compartilhar boleto", '', path + file)
          .then( s => {
            this.file.removeFile(path, file);
          })
          callback()
        }
      }]
    });
    await actionSheet.present();
  }


  b64toBlob(b64Data, contentType) {
      contentType = contentType || '';
      var sliceSize = 512;
      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          var slice = byteCharacters.slice(offset, offset + sliceSize);

          var byteNumbers = new Array(slice.length);
          for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
          }

          var byteArray = new Uint8Array(byteNumbers);

          byteArrays.push(byteArray);
      }

    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }

  public saveBase64(base64:string, name:string, dir ):Promise<string>{
    return new Promise((resolve, reject)=>{
      var realData = base64.split(",")[1]
      let blob=this.b64toBlob(realData, 'application/pdf')

      this.file.writeFile(dir, name, blob)
      .then(()=>{
        resolve(dir+name);
      })
      .catch((err)=>{
        console.log('error writing blob')
        reject(err)
      })
    })
  }

  converteDecimal(valor) {

    if(!valor || valor == "") valor = "0";

    let res = valor.replace(`,`, ``);
    let decimals = res.substring(res.length - 2, res.length)

    return res.substring(0, res.length-2) + `.` + decimals;
  }

  formataData(date){
    let d = new Date(date);
    return d.toLocaleDateString(); 
  }

  floatToMonetary(valor){

    var inteiro = null, decimal = null, c = null, j = null;
    var aux = new Array();

    valor = "" + valor;
    c = valor.indexOf(".",0);
    //encontrou o ponto na string
    if(c > 0){
       //separa as partes em inteiro e decimal
       inteiro = valor.substring(0,c);
       decimal = valor.substring(c+1,valor.length);
    }else{
       inteiro = valor;
    }
       //pega a parte inteiro de 3 em 3 partes
    for (j = inteiro.length, c = 0; j > 0; j-=3, c++){
       aux[c]=inteiro.substring(j-3,j);
    }

    //percorre a string acrescentando os pontos
    inteiro = "";
    for(c = aux.length-1; c >= 0; c--){
       inteiro += aux[c]+'.';
    }
    //retirando o ultimo ponto e finalizando a parte inteiro
    inteiro = inteiro.substring(0,inteiro.length-1);

    if(!decimal)
        decimal = "00";
        
    valor = "R$ "+inteiro+","+decimal.substring(0,2);

    return valor;
 }


}