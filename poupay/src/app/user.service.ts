import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
// import 'rxjs/add/operator/map';
// import { map } from 'rxjs/operators';
import { HTTP } from '@ionic-native/http/ngx';
import { Plugins } from '@capacitor/core';

const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})

export class UserService {

  constructor(public http: HTTP) { }

  auth(email, password, callback){
    
    this.http.post('http://poupay.us-east-2.elasticbeanstalk.com/users/authenticate', {email: email, password:password}, {})
    .then(data => callback(JSON.parse(data.data), null) )
    .catch( error => {
      callback(null, JSON.stringify(error))
    });
  }

  async get(callback) {
    let headers = {
      "Authorization": "Bearer " + await this.getToken()
    }
    this.http.get('http://poupay.us-east-2.elasticbeanstalk.com/users/current', {}, headers)
      .then(data => callback(JSON.parse(data.data), null) )
      .catch( error => {
        callback(null, JSON.stringify(error))
      });
  }


  async getTotalPontos(callback) {
    // let headers: Headers = new Headers()
    // headers.append('Authorization', "Bearer " + await this.getToken());


    let headers = {
      "Authorization": "Bearer " + await this.getToken()
    }
    this.http.get('http://poupay.us-east-2.elasticbeanstalk.com/pontos/totalPontos', {}, headers)
    .then(data => callback(JSON.parse(data.data), null) )
    .catch( error => {
      callback(null, JSON.stringify(error))
    });

    // this.http.get('http://poupay.us-east-2.elasticbeanstalk.com/pontos/totalPontos', {headers: headers}).subscribe({
    //   next: data => callback(data.json(), null),
    //   error: error => {
    //     console.log(error);
        
    //     callback(null, error.json().message ? error.json().message : "Erro desconhecido!")
    //   }
    // });

  }

  async debitaPontos(body, callback) {
    // let headers: Headers = new Headers()
    // headers.append('Authorization', "Bearer " + await this.getToken());

    let headers = {
      "Authorization": "Bearer " + await this.getToken()
    }
    this.http.put('http://poupay.us-east-2.elasticbeanstalk.com/pontos/debitar', body, headers)
      .then(data => callback(JSON.parse(data.data), null) )
      .catch( error => {
        callback(null, JSON.stringify(error))
      });

  }


  async getSaldoAtual(callback) {
    // let headers: Headers = new Headers()
    // headers.append('Authorization', "Bearer " + await this.getToken());

    let headers = {
      "Authorization": "Bearer " + await this.getToken()
    }
        // this.http.get('http://poupay.us-east-2.elasticbeanstalk.com/carteiras/getSaldo', {headers: headers}).subscribe({
    //   next: data => callback(data.json(), null),
    //   error: error => {
    //     console.log(error);
        
    //     callback(null, error.json().message ? error.json().message : "Erro desconhecido!")
    //   }
    // });

    this.http.get('http://poupay.us-east-2.elasticbeanstalk.com/carteiras/getSaldo', {}, headers)
    .then(data => callback(JSON.parse(data.data), null) )
    .catch( error => {
      callback(null, JSON.stringify(error))
    });
  }

  async getToken() {
    const ret = await Storage.get({ key: 'user' });
    const user = JSON.parse(ret.value);

    if (user) {
      return user.token;
    }
   }
}
