
import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { TabsPage } from '../tabs/tabs.page';
import { Router } from '@angular/router';

import {UserService} from '../user.service';

import { Plugins } from '@capacitor/core';

const { Storage } = Plugins;

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public formLogin = {
    email: '',
    senha: ''
  }
  
  constructor(private router: Router,
              public alertCtrl: AlertController,
              private userService: UserService )
  {
// this.userData.getUserLogged().then( user => {
//   user = `william`;
//   if(user && user.length > 0) this.navCtrl.push(AppContentPage);
// });
}

  async ngOnInit() {
    const ret = await Storage.get({ key: 'user' });
    const user = JSON.parse(ret.value);

    if (user) {
      this.router.navigate(['/tabs/objetivos']);
    }
  }


  async presentAlert(message) {
    const alert = await this.alertCtrl.create({
      header: 'Ops',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  async login(){

    this.userService.auth(this.formLogin.email, this.formLogin.senha, (data, err) => {
        console.log(err);
        
      if (err) {
        this.presentAlert(err);
      } else {
        Storage.set({
          key: 'user',
          value: JSON.stringify({
            nome: data.nome,
            token: data.token
          })
        }).then(
          v => {
            this.router.navigate(['/tabs/objetivos']);
          }
        );
 
      }
    });

  }

}


