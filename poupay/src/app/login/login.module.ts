import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginPageRoutingModule } from './login-routing.module';

import { LoginPage } from './login.page';


// import {MatInputModule} from '@angular/material/input'
// import {MatFormFieldModule} from '@angular/material/form-field'



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginPageRoutingModule
  ],
  declarations: [LoginPage]
})

export class LoginPageModule {}


// import { Component } from '@angular/core';

// import { NavController, AlertController } from 'ionic-angular';

// import {ConfiguracoesPage} from '../configuracoes/configuracoes';
// import {AppContentPage} from '../app-content/app-content';

// import {UserData} from '../../providers/user-data';

// import {Funcionarios} from '../../providers/funcionarios';

// @Component({
//   selector: 'page-home',
//   templateUrl: 'home.html'
// })
// export class HomePage {
//   public configuracoesPage = ConfiguracoesPage;
//   public appContentPage = AppContentPage;

//   public formLogin = {
//     usuario: '',
//     senha: ''
//   }

//   public params = {
//     chamadoDoLogin: true
//   }

//   constructor(public navCtrl: NavController,
//               public userData: UserData,
//               public funcionarios: Funcionarios,
//               public alertCtrl: AlertController )
//   {
//     this.userData.getUserLogged().then( user => {
//       user = `william`;
//       if(user && user.length > 0) this.navCtrl.push(AppContentPage);
//     });
//   }

//   login(){
//     this.funcionarios.verificaLogin({nome: this.formLogin.usuario, senha: this.formLogin.senha }, (erro) => {
//       if(erro){
//         let alert = this.alertCtrl.create({
//           title: 'Atenção',
//           subTitle: erro,
//           buttons: ['OK']
//         });

//         alert.present();
//       } else {
//         this.userData.setUserLogged(this.formLogin.usuario);
//         this.navCtrl.push(AppContentPage);
//       }
//     })
//   }

// }
