const dateUtils = require("../_helpers/dateUtils");
const db = require('../_helpers/db');
const { Objetivo, ObjetivoParcela, User } = db;
const mongoose = require('mongoose');
const boletoService = require('../boleto/boleto.service');

module.exports = {
    getAll,
    getById,
    create,
    delete: _delete,
    getBoletoParcela
}

async function getAll(id_user) {
    if (!validaUsuario(id_user)) throw "Id do usuario invalido!"
    const objetivos = await Objetivo.find( {id_user: id_user});

    const promises = objetivos.map(async (objetivo) => {
        objetivo.parcelas = await ObjetivoParcela.find({id_objetivo: objetivo._id});
    })

    await Promise.all(promises);
    return objetivos;
}

async function getById(id_user, id) {
    if (!validaUsuario(id_user)) throw "Id do usuario invalido!"
    const objetivo = await Objetivo.findOne( { id_user: id_user, _id: id } );
    objetivo.parcelas = await ObjetivoParcela.find({id_objetivo: objetivo._id});

    return objetivo;
}

async function create(id_user, objetivoParam) {
    if (!validaUsuario(id_user)) throw "Id do usuario invalido!"

    const user = await User.findById(id_user);
    if (!user) throw "Usuario nao encontrado!"

    id_user = user._id;
    objetivoParam.id_user = id_user;

    const objetivo = new Objetivo(objetivoParam);

    await objetivo.save();

    const { valorTotal, valorEntrada, valorMensal, dataPrimeiraParcela, diaVencimento } = objetivoParam;
    let meses = ( valorTotal - valorEntrada ) / valorMensal;

    if (valorEntrada > 0) {
        await boletoService.create(id_user, {
            id_objetivo: objetivo._id,
            id_user: id_user,
            data_emissao: Date.now(),
            data_vencimento: Date.now() + ( 3600 * 1000 * 24 * 3 ),
            nosso_numero: "123456", //alterar isso depois!
            valor: valorEntrada,
            id_parcela: objetivo._id,
            valor_pago: 0
        });
    }

    let objetivoParcelas = [];

    for (let i = 0; i < meses ; i++) {
        objetivoParcelas.push({
            id_objetivo: objetivo._id,
            data: i === 0 ? dataPrimeiraParcela : dateUtils.calculaDataParcela(i, diaVencimento),
            valor: valorMensal,
            valorPago: 0,
            dataPago: new Date(0),
            id_parcela: i
        });
    }

    const promises = objetivoParcelas.map(async (objetivoParcela) => {
        const objetivoParcela1 = new ObjetivoParcela(objetivoParcela);
        await objetivoParcela1.save();
    });

    await Promise.all(promises);

    return objetivo;
}

async function getBoletoParcela(id_parcela, callback){
    // let user = User.findById(id_user);
    let parcela = await ObjetivoParcela.findById(id_parcela);
    
    boletoService.generate({
        data_emissao: Date.now(),
        data_vencimento: parcela.data,
        nosso_numero: "123456", //alterar isso depois!
        valor: parcela.valor
    }, id_parcela.toString() + ".pdf", (response) => {
        callback(response)
    });
}

//TODO: Criar função para alterar os objetivos com recalculo das parcelas

async function _delete(id_user, id) {
    await Objetivo.findOneAndDelete({ id_user: id_user, _id: id });
    await ObjetivoParcela.deleteMany( { id_objetivo: id } );
}

function validaUsuario(id_user) {
    return mongoose.mongo.ObjectID.isValid(id_user);
}
