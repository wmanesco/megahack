const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    id_user: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'users'
    },
    valorTotal: {
        type: Number,
        min: 0.01,
        required: true
    },
    valorMensal: {
        type: Number,
        min: 0.01,
        required: true
    },
    valorEntrada: {
        type: Number,
        min: 0,
        required: false
    },
    diaVencimento: {
        type: Number,
        min: 1,
        max: 31,
        required: true
    },
    nome: {
        type: String,
        minlength: 1,
        maxlength: 50,
        required: true
    },
    categoria: {
        type: String,
        enum: ['SAUDE', 'FINANCEIRO', 'AUTOMOVEL', 'EDUCACAO', 'VIAGENS', 'SOCIAL', 'LAZER', 'ESPORTE', 'OUTROS'],
        required: true
    },
    parcelas: {
        type: Array
    }

});

module.exports = mongoose.model('Objetivo', schema);