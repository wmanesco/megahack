const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    id_objetivo: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Objetivo',
        required: true
    },
    data: {
        type: Date,
        required: true
    },
    valor: {
        type: Number,
        required: true
    },
    valorPago: {
        type: Number,
        required: false
    },
    dataPago: {
        type: Date,
        required: false
    },
    pontosGanhos: {
        type: Number
    }
});

module.exports = mongoose.model('ObjetivoParcela', schema);