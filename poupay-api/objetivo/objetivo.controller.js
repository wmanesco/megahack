const express = require('express');
const router = express.Router();
const servicoObjetivo = require('./objetivo.service');

router.post('/', criaObjetivo);
//router.put();
router.get('/:id', getById);
router.get('/', getAll);
router.delete('/:id', _delete);
router.get('/boleto/:id_parcela', getBoletoParcela)

module.exports = router;

async function criaObjetivo(req, res, next) {
    servicoObjetivo.create(req.user.sub, req.body)
        .then(objetivo => objetivo ? res.json(objetivo) : res.sendStatus(500))
        .catch(err => next(err));
}

async function getAll(req, res, next) {
    servicoObjetivo.getAll(req.user.sub)
        .then(objetivos => objetivos ? res.json(objetivos) : res.status(204))
        .catch(err => next(err));
}

async function getById(req, res, next) {
    servicoObjetivo.getById(req.user.sub, req.params.id)
        .then(objetivo => objetivo ? res.json(objetivo) : res.status(404))
        .catch(err => next(err));
}

async function _delete(req, res, next) {
    servicoObjetivo.delete(req.user.sub, req.params.id)
        .then(r => res.status(200))
        .catch(err => next(err));
}

async function getBoletoParcela(req, res, next) {
    servicoObjetivo.getBoletoParcela(req.params.id_parcela, (response) => {
        if(response) {
            res.json(response)
            res.status(200);
        } else {
            next("Erro ao gerar boleto")
        }
    })
        // .then(r => res.status(200))
        // .catch(err => next(err));
}