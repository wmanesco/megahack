const config = require('../config.json');
const mongoose = require('mongoose');
const fs = require(`fs`);
const certFileBuf = fs.readFileSync("cert.pem");
const connectionOptions = {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    ssl: true,
    sslValidate: true,
    sslCA: certFileBuf,
};
mongoose.connect(process.env.MONGODB_URI || config.connectionString, connectionOptions);
mongoose.Promise = global.Promise;

module.exports = {
    User: require('../users/user.model'),
    Carteira: require('../carteira/carteira.model'),
    Boleto: require('../boleto/boleto.model'),
    Objetivo: require('../objetivo/objetivo.model'),
    ObjetivoParcela: require('../objetivo/parcelas/parcela.model'),
    Pontos: require('../pontos/pontos.model')
};