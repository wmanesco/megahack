module.exports = { calculaDataParcela };

function calculaDataParcela(mes, dia) {
    let dateBase = new Date();
    let mesBase = dateBase.getMonth();
    let anoBase = dateBase.getFullYear();

    while (mes + mesBase > 12) {
        anoBase++;
        mes -= 12;
    }

    mes += mesBase

    return new Date(anoBase, mes, dia);//DateTime.fromDate(anoBase, mes, dia);
}