const express = require('express');
const router = express.Router();
const boletoService = require('./boleto.service');

// routes
router.get('/', getAll);
router.get('/:id', getById);
router.put('/:id', update);
router.delete('/:id', _delete);
router.post('/', create);
router.post('/pagar/:id', pay);

module.exports = router;

function getAll(req, res, next) {
    boletoService.getAll()
        .then(boletos => res.json(boletos))
        .catch(err => next(err));
}

function getById(req, res, next) {
    boletoService.getById(req.params.id)
        .then(boleto => boleto ? res.json(boleto) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    boletoService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    boletoService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function create(req, res, next) {
    boletoService.create(req.user.sub,req.body)
        .then(boleto => res.json(boleto))
        .catch(err => next(err));
}

function pay(req, res, next) {
    boletoService.pay(req)
        .then(() => res.json({}))
        .catch(err => next(err));
}
