const db = require('../_helpers/db');
const Boleto = db.Boleto;
const User = db.User;
const Parcela = db.ObjetivoParcela;
const NodeBoleto = require('node-boleto').Boleto;
const mongoose = require('mongoose');
const carteiraService = require('../carteira/carteira.service');
const pontosManager = require('../pontos/pontosManager');
const pdf = require("html-pdf");
const fs = require('fs')

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete,
    pay,
    generate
};

async function getAll() {
    return await Boleto.find();
}

async function getById(id) {
    return await Boleto.findById(id);
}

async function create(id_user, boletoParam) {

    boletoParam.id_user = id_user;
    if (!mongoose.Types.ObjectId.isValid(boletoParam.id_user)) throw "Id do usuario invalido"

    let user = await User.findById(boletoParam.id_user);
    if (!user) throw "Usuario nao encontrado"

    //todo validar objetivos
    const boleto = new Boleto(boletoParam);

    let nodeBoleto = new NodeBoleto({
        'banco': "santander", 
        'data_emissao': boleto.data_emissao,
        'data_vencimento': boleto.data_vencimento,
        'valor': boleto.valor, // R$ 15,00 (valor em centavos)
        'nosso_numero': boleto.nosso_numero.toString(),
        'numero_documento': boleto.nosso_numero.toString(),
        'cedente': "Poypay!",
        'cedente_cnpj': "11111111111111", // sem pontos e traços
        'agencia': "3978",
        'codigo_cedente': "6404154", // PSK (código da carteira)
        'carteira': "102"
    });

    boleto.linha_digitavel = nodeBoleto['linha_digitavel']

    await boleto.save();
    return boleto;
}

function generate(boleto, fileName, callback) {

    let nodeBoleto = new NodeBoleto({
        'banco': "santander", 
        'data_emissao': boleto.data_emissao,
        'data_vencimento': boleto.data_vencimento,
        'valor': boleto.valor * 100, // R$ 15,00 (valor em centavos)
        'nosso_numero': boleto.nosso_numero,
        'numero_documento': boleto.nosso_numero,
        'cedente': "Poypay!",
        'cedente_cnpj': "11111111111111", // sem pontos e traços
        'agencia': "3978",
        'codigo_cedente': "6404154", // PSK (código da carteira)
        'carteira': "102"
    });

    let pdfFile = "";
    nodeBoleto.renderHTML(function(html){
        let options = {
            width: '24cm',
            height: "12in",
            type: `pdf`,
            orientation: 'portrait',
            border: {
                "top": "2px",            // default is 0, units: mm, cm, in, px
                "right": "2px",
                "bottom": "2px",
                "left": "3px"
              }
        };
    
        pdf.create(html, options).toFile(fileName, function (err, data) {
            if (err) {
                console.log(err);
            } else {
                pdfFile = fs.readFileSync(fileName);
                callback({linhaDigitavel: nodeBoleto['linha_digitavel'], pdf: pdfFile.toString('base64')});
            }
        });
    });
}

async function update(id, boletoParam) {
    const boleto = await Boleto.findById(id);

    // validate
    if (!boleto) throw 'boleto not found';

    // copy boletoParam properties to boleto
    Object.assign(boleto, boletoParam);

    await boleto.save();
    return boleto;
}

async function _delete(id) {
    await Boleto.findByIdAndRemove(id);
}

async function pay(req) {
    const parcela = await Parcela.findById(req.params.id);

    // req.body.dataPago = new Date(req.body.dataPago);

    Object.assign(parcela, req.body);
    let dadosCarteira = {
        'id_user': req.user.sub,
        'valor_transacao': req.body.valorPago,
        'operacao': 'Credito'
    };

    parcela.pontosGanhos = 30;
    await pontosManager.addPontos(req.user.sub, 30, 'Pagamento boleto');
    await parcela.save();
    await carteiraService.create(req, dadosCarteira);

    return parcela;
}
