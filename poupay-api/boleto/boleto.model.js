const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    id_objetivo: { type: mongoose.SchemaTypes.ObjectId, ref: 'Objetivo' }, //alterar para objectID quando tiver obetivos pronto.
    id_user: { type: mongoose.SchemaTypes.ObjectId, required: true, ref: 'users' },
    linha_digitavel: String,
    data_emissao: { type: Date, required: true, default: Date.now },
    data_vencimento: { type: Date, required: true },
    data_pagamento: Date,
    nosso_numero: { type: Number, required: true },
    valor: { type: Number, required: true },
    valor_pago: { type: Number, required: true },
    id_parcela: { type: mongoose.SchemaTypes.ObjectId, required: true, ref: 'ObjetivoParcela' }
});

module.exports = mongoose.model('Boleto', schema);