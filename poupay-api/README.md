## API
##### Breve documentação da API Poupay!


## Users: Registro e autenticação

**POST** /users/register

        { "username": string,
	      "password": string,
	      "firstName": string,
	      "lastName": string }
  
Retorna 200 se sucesso e nada no body.

---

**POST** /users/authenticate

	{ "username": string, 
	  "password": string }

Retorna o usuario e uma tag "token" que deve ser enviado no header das proximas requisições da seguinte forma:\
`"Authorization: Bearer <token>"`