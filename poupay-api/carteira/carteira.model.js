const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    id_user: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'users'
    },
    valor_transacao: {
        type: Number
    },
    operacao: {
        type: String,
        maxlength: 20
    }
});

module.exports = mongoose.model('Carteira', schema);