const express = require('express');
const router = express.Router();
const carteiraService = require('./carteira.service');

// routes
router.get('/', getAll);
router.get('/getSaldo', getSaldo)
router.get('/:id', getById);
router.put('/:id', update);
router.delete('/:id', _delete);
router.post('/', create);

module.exports = router;

function getAll(req, res, next) {
    carteiraService.getAll()
        .then(carteiras => res.json(carteiras))
        .catch(err => next(err));
}

function getById(req, res, next) {
    carteiraService.getById(req.params.id)
        .then(carteira => carteira ? carteira.json(carteira) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    carteiraService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    carteiraService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function create(req, res, next) {
    carteiraService.create(req, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function getSaldo(req, res, next) {
    carteiraService.getSaldo(req.user.sub)
        .then((saldo) => res.json(saldo))
        .catch(err => next(err));
}