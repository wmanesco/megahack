const config = require('../config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('../_helpers/db');
const { Carteira, User } = db;


module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete,
    getSaldo: _getSaldo
};

async function getAll() {
    
    return await Carteira.find();
}

async function getById(id) {
    return await Carteira.findById(id);
}

async function create(req, carteiraParametros) {

    if (!carteiraParametros.hasOwnProperty('id_user')) {
        carteiraParametros.id_user = req.user.sub;
    }

    const carteira = new Carteira(carteiraParametros);

    await carteira.save(carteiraParametros);
}

async function update(id, carteiraParametros) {
    const carteira = await Carteira.findById(id);

    if (!carteira) throw 'Registro nao encontrado!';

    Object.assign(carteira, carteiraParametros);

    await carteira.save();
}

async function _delete(id) {
    await Carteira.findByIdAndRemove(id);
}

async function _getSaldo(id_user){
    let saldo = 0;
    let operacoes = await Carteira.find({id_user: id_user});

    operacoes.forEach(operacao => {
        console.log(operacao);
        
        if (operacao.valor_transacao) {
            if(operacao.operacao == "Credito") {
                saldo += operacao.valor_transacao;
            } else if (operacao.operacao == "Debito") {
                saldo -= operacao.valor_transacao;
            }   
        }
    });

    return {saldo: saldo}
}
