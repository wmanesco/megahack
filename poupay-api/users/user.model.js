const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const validateEmail = function(email) {
    let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
};


const schema = new Schema({
    email: { type: String,
             unique: true,
             required: true,
             trim: true,
             validate: [validateEmail, 'Por favor insira um e-mail valido!'],
             match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Por favor insira um e-mail valido!'] 
            },
    hash: { type: String, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    cpf: { type: String, unique: true },
    whatsapp: { type: String },
    pais: { type: String },
    estado: { type: String },
    pontos_total: { type: Number },
    createdDate: { type: Date, default: Date.now }
});

schema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {
        delete ret._id;
        delete ret.hash;
    }
});

module.exports = mongoose.model('User', schema);