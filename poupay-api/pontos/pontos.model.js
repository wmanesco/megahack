const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    id_user: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'users',
        required: true
    },
    pontos: {
        type: Number,
        min: 0.01,
        required: true
    },
    tipo: {
        type: String,
        enum: ['C', 'D'],
        required: true
    },
    operacao: {
        type: String,
        maxlength: 50,
        required: true
    }
});

module.exports = mongoose.model('pontos', schema);