const db = require('../_helpers/db');
const { Pontos, User } = db;

module.exports = {
    addPontos,
    removePontos,
    getTotalPontos
}

async function addPontos(id_user, pontos, operacao) {
    const user = await User.findById(id_user);

    const pontosTransacao = new Pontos( { id_user: id_user, tipo: 'C', pontos: pontos, operacao: operacao } );

    if (!user.pontos_total)
        user.pontos_total = 0

    user.pontos_total += pontos;

    await user.save();
    await pontosTransacao.save();
    return pontosTransacao;
}

async function removePontos(id_user, pontos, operacao) {
    const user = await User.findById(id_user);

    const pontosTransacao = new Pontos( { id_user: user._id, tipo: 'D', pontos: pontos, operacao: operacao } );

    if (!user.pontos_total)
        user.pontos_total = 0

    user.pontos_total -= pontos;

    await user.save();
    await pontosTransacao.save();
    return pontosTransacao;
}

async function getTotalPontos(id_user) {
    const user = await User.findById(id_user);
    return user.pontos_total;
}
