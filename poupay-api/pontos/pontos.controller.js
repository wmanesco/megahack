const express = require('express');
const router = express.Router();
const pontosService = require('./pontos.service');

router.get('/totalPontos', getTotalPontos);
router.put('/debitar', removePontos);

function getTotalPontos(req, res, next) {
    pontosService.getTotal(req.user.sub)
        .then(pontos => pontos ? res.json(pontos): res.status(204))
        .catch(err => next(err));
}

function removePontos(req, res, next) {
    pontosService._remove(req.user.sub, req.body)
        .then(pontos => pontos ? res.json(pontos) : res.status(204))
        .catch(err => next(err));
}

module.exports = router;