const pontosManager = require('./pontosManager');

module.exports = {
    _remove,
    getTotal
}

async function _remove(id_user, {pontos, operacao}) {
    return await pontosManager.removePontos(id_user, pontos, operacao);
}

async function getTotal(id_user) {
    const pontos = await pontosManager.getTotalPontos(id_user);
    return { totalPontos: pontos }
}