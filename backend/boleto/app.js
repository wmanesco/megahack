let Boleto = require('node-boleto').Boleto;
let pdf = require("html-pdf");

let boleto = new Boleto({
  'banco': "santander", // nome do banco dentro da pasta 'banks'
  'data_emissao': new Date(),
  'data_vencimento': new Date(new Date().getTime() + 5 * 24 * 3600 * 1000), // 5 dias futuramente
  'valor': 1500, // R$ 15,00 (valor em centavos)
  'nosso_numero': "1234567",
  'numero_documento': "121212",
  'cedente': "Poypay!",
  'cedente_cnpj': "11111111111111", // sem pontos e traços
  'agencia': "3978",
  'codigo_cedente': "6404154", // PSK (código da carteira)
  'carteira': "102"
});

console.log("Linha digitável: " + boleto['linha_digitavel'])

boleto.renderHTML(function(html){
    let options = {
        width: '24cm',
        height: "12in",
        type: `pdf`,
        orientation: 'portrait',
        border: {
            "top": "2px",            // default is 0, units: mm, cm, in, px
            "right": "2px",
            "bottom": "2px",
            "left": "3px"
          }
    };

    pdf.create(html, options).toFile("boleto.pdf", function (err, data) {
        if (err) {
            console.log(err);
        } else {
            console.log("File created successfully");
        }
    });
});