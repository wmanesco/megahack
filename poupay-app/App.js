import * as React from 'react';
import { AsyncStorage } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import SafeAreaView from 'react-native-safe-area-view'

import Login from './src/components/Login';
import Register from './src/components/Register';
import AppIndex from './src/components/Index';


const InitialScreenStack = createStackNavigator();
// var isLoggedIn = false;
function InitialScreenStackScreen() {
  let headerOptions = {
    headerShown: true, 
    headerStyle: {
      backgroundColor: '#f4511e',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    } }

  return ( //aqui adicionar as telas relacionadas ao login (esqueci a senha, registro, etc)
    <InitialScreenStack.Navigator>
      <InitialScreenStack.Screen name="Login" component={Login} options={{ headerShown: false }} />
      <InitialScreenStack.Screen name="Registrar" component={Register} options={headerOptions} />
      <InitialScreenStack.Screen name="AppIndex" component={AppIndex} options={headerOptions} />
    </InitialScreenStack.Navigator>
  );
}


export default function App() {
  const [user, setUser] = React.useState(null);
  
  AsyncStorage.getItem('@PoupayApp:token', (err, result) => {
    if(result) setUser(result);
  });

  return (
    <NavigationContainer>
      {user ? (
        <AppIndex/>
      ) : (
        <InitialScreenStackScreen/>
      )}
    </NavigationContainer>
  );
}
