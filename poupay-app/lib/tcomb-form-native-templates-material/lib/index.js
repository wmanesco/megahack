import textbox from './textbox'
import checkbox from './checkbox'
import select from './select'
import datepicker from './datepicker.android'
import struct from './struct'
import list from './list'

// console.log(JSON.stringify(textbox)};


export default {
  textbox: textbox,
  checkbox: checkbox,
  select: select,
  datepicker: datepicker,
  struct: struct,
  // list: list
};
