import React, { Component } from 'react'
import { ImageBackground, Alert, Text, View, Image, TextInput, TouchableOpacity, TouchableHighlight } from 'react-native'

import styles from '../styles/Register'
import Users from "./../helpers/users";


// const templates = require(  '../../lib/tcomb-form-native-templates-material/lib/index' );
// console.log(JSON.stringify(templates));

// import templates from 'tcomb-form-native-templates-material';

const t = require('tcomb-form-native');

// const Form = t.form.Form;

// Form.templates = {textbot: textbox};
//   textbox: require("../../lib/tcomb-form-native-templates-material/lib/textbox"),
//   checkbox: require("../../lib/tcomb-form-native-templates-material/lib/checkbox"),
//   select: require("../../lib/tcomb-form-native-templates-material/lib/select"),
//   datepicker: require("../../lib/tcomb-form-native-templates-material/lib/datepicker.android"),
//   struct: require("../../lib/tcomb-form-native-templates-material/lib/struct"),
//   list: require("../../lib/tcomb-form-native-templates-material/lib/list"),
// };

export default class Register extends Component { 

    constructor(props) {
        super(props);
        this.state = {
            pessoa: {}
        };
        
        this.validate = null;

        this.samePassword = t.refinement(t.String, (s) => {
            return s == this.state.pessoa.senha;
        })

        this.RegisterForm = t.struct({
            nome: t.String,      
            sobrenome: t.String,  
            email: t.String,    
            cpf: t.String,       
            senha: t.String,        
            confirmarSenha: this.samePassword,        
        });

        // this.RegisterForm.templates = Form.templates;

        this.options = {
            auto: 'placeholders',
            fields: {
                senha: {
                    password: true,
                    secureTextEntry: true,
                    error: "",
                },
                confirmarSenha: {
                    password: true,
                    secureTextEntry: true,
                    error: "Senhas diferentes!",
                },
                cpf: {
                    placeholder: "CPF"
                }
            }
        }
    }
    
    onChange(pessoa) {
        this.setState({ pessoa });
        if(pessoa.confirmarSenha != null && pessoa.confirmarSenha != "") {
            this.validate = this.refs.form.getValue();
        }
    }

    onPress() {
        var value = this.refs.form.getValue();
        console.log(value);
        
        if (value) { // if validation fails, value will be null
            let users = new Users();
            let user = {
                firstName: value.nome,
                lastName: value.sobrenome,
                email: value.email,
                cpf: value.cpf,
                password: value.senha
            }
            users.register(user, (data, err) => {
                
              if (err) {
                Alert.alert("Ops...", err)
              } else {
                Alert.alert("Sucesso", "Registro realizado com sucesso!")
                this.props.navigation.goBack()
              }
    
            });
        }
    }

    render() {
        return (
          <View style={styles.container}>
            {/* display */}
            <Form
              ref="form"
              type={this.RegisterForm}
              value={this.state.pessoa}
              options={this.options}
              onChange={(v) => this.onChange(v)}
            />
            <TouchableHighlight disabled={this.validate? false: true} style={styles.button} onPress={() => this.onPress()}>
              <Text style={styles.buttonText}>Registrar</Text>
            </TouchableHighlight>
          </View>
        );
      }
}