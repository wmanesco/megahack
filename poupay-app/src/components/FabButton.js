import React, { Component } from 'react'
import { Alert, View, Text, StyleSheet, TouchableWithoutFeedback } from 'react-native'

import styles from '../styles/FabButton'

//const image = require( '../../images/icon-add.png' )

export default class FabButton extends Component {

    add = () => {
        Alert.alert( 'add' )
    }

    render() {
        return (
            <View style={[styles.container, this.props.style]}>
                <TouchableWithoutFeedback
                onPress={this.add}
                >
                    <Text style={styles.text}>+</Text> 
                </TouchableWithoutFeedback>
            </View>
        )
    }
}
