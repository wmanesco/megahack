import React, { Component } from 'react';
import { ImageBackground, Text, View, FlatList, TouchableWithoutFeedback, Image, Alert } from 'react-native';
import Swipeable from 'react-native-gesture-handler/Swipeable'

import styles    from '../styles/Objetivos'
import FabButton from './FabButton'

const iconsaude     = require( '../../images/icons/icon-saude.png' )
const iconsocial    = require( '../../images/icons/icon-social.png' )
const iconautomovel = require( '../../images/icons/icon-automovel.png' )
const iconviagem    = require( '../../images/icons/icon-viagem.png' )
const iconeditar    = require( '../../images/icons/icon-editar.png' )
const iconremove    = require( '../../images/icons/icon-remove.png' )

export default function Objetivos() {

    const objetivos = [
        { id: '1', title: 'Viajar para o Canadá'                         , description: 'Descrição do objetivo 1', icon: iconviagem },
        { id: '2', title: 'Comprar um carro'                             , description: 'Descrição do objetivo 2', icon: iconautomovel },
        { id: '3', title: 'Doação ao hospital do câncer teste multi-line', description: 'Descrição do objetivo 3', icon: iconsaude },
        { id: '4', title: 'Fazer uma festa em família'                   , description: 'Descrição do objetivo 3', icon: iconsocial },
        { id: '5', title: 'Doação ao hospital do câncer'                 , description: 'Descrição do objetivo 3', icon: iconsaude },
        { id: '6', title: 'Doação ao hospital do câncer'                 , description: 'Descrição do objetivo 3', icon: iconsaude },
        { id: '7', title: 'Doação ao hospital do câncer'                 , description: 'Descrição do objetivo 3', icon: iconsaude },
        { id: '8', title: 'Doação ao hospital do câncer'                 , description: 'Descrição do objetivo 3', icon: iconsaude },
        { id: '9', title: 'Doação ao hospital do câncer'                 , description: 'Descrição do objetivo 3', icon: iconsaude },
    ];

    return (
        <View style={styles.container}>
            <FlatList
            data={objetivos}
            keyExtractor={ item => item.id }
            renderItem={ ({item}) => (
                <ListItem
                data={item}
                />
            ) }
            />
            <FabButton style={{ bottom: 10, right: 10 }} />
        </View>
    )
}

function ListItem( { data } ) {

    function RightActions() {
        return (
          <View style={styles.buttonRight}>
              <Image source={iconremove}/>
          </View>
        )
    }

    function Edit() {
        Alert.alert( 'edit' )
    }

    return (
    <Swipeable
    renderRightActions={RightActions}
    >
        <View style={styles.postContainer}>
            <Image source={data.icon} style={styles.icon}></Image>
            <Text style={styles.postTitle}>{ data.title }</Text>
        </View>
        <View style={styles.buttonEdit}>
            <TouchableWithoutFeedback
            onPress={Edit}
            >
                <Image source={iconeditar} style={styles.iconedit}></Image>
            </TouchableWithoutFeedback>
        </View>
    </Swipeable>
    )
}
