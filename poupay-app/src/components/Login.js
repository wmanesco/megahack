import React, { Component, TouchableHighlight } from 'react'
import { ImageBackground, Alert, Text, View, Image, TextInput, TouchableOpacity } from 'react-native'

import Users from "./../helpers/users";

import styles from '../styles/Login'

const image = require( '../../images/background-login.png' )

export default class App extends Component {
    constructor(props) {
      super(props);
      this.state = {email: "", password: ""}
    }

    clicou = () => {
        Alert.alert( 'PouPay', 'Entrar' )
        this.props.navigation.navigate("AppIndex")
    }

    register() {
      this.props.navigation.navigate("Registrar")
    }

    autenticar = () => {
        
        let users = new Users();
        users.authenticate(this.state.email, this.state.password, (data, err) => {
            
          if (err) {
            Alert.alert("Ops...", err)
          } else {
            this.props.navigation.navigate("AppIndex")
          }

        });

    }

    render() {
        
        return (
            <ImageBackground source={image} style={styles.image}>
            <View style={styles.container}>
      
              <Image
                source={require( '../../images/logo.jpeg' )}
                style={styles.logo}
              />
      
              <TextInput
                style={styles.input}
                placeholder='Email'
                placeholderTextColor="#000" 
                onChangeText={text => this.setState({ email: text })}
              />
                
              <TextInput
                style={styles.input}
                secureTextEntry={true}
                placeholder='Senha'
                placeholderTextColor="#000" 
                onChangeText={text => this.setState({ password: text })}
              />
              
              <TouchableOpacity
                style={styles.button}
                onPress={ () => {this.autenticar()} }
              >
                <Text style={styles.buttonText}>Acessar</Text>
              </TouchableOpacity>
      
              <Text style={styles.buttonTextRegister}>Esqueci minha senha</Text>
      
              <Text style={styles.text}>Ou faça seu login com</Text>
      
              <View style={styles.fixToText}>
                <TouchableOpacity
                  style={styles.buttonFacebook}
                  onPress={ () => {this.clicou()} }
                >
                  <Text style={styles.buttonText}>Facebook</Text>
                </TouchableOpacity>
        
                <TouchableOpacity
                  style={styles.buttonGoogle}
                  onPress={ () => {this.clicou()} }
                >
                  <Text style={styles.buttonText}>Google+</Text>
                </TouchableOpacity>
              </View>
              <View>
                <TouchableOpacity style={styles.buttonTextRegister}  onPress={ () => this.register()}>
                  <Text>Ainda não possui cadastro? Crie sua conta!</Text>
                </TouchableOpacity>
              </View>
              
            </View>
          </ImageBackground>
        )
    }
}