import React, { Component } from 'react'
// import { createDrawerNavigator } from '@react-navigation/drawer';
import Objetivos from './Objetivos';
import { BottomNavigation } from 'react-native-material-ui';
import { View } from 'react-native'

// const MainDrawer = createDrawerNavigator();
export default class AppIndex extends Component  { //aqui adicionar as telas de dentro do app.
  
  constructor(props) {
    super(props);
    this.state = {
        active: ""
    }
  }

  render() { 
    // <View>
    return (
      <View>
      <BottomNavigation active={this.state.active} hidden={false} >
        <BottomNavigation.Action
            key="Objetivos"
            // icon="Objetivos"
            label="Objetivos"
            onPress={() => this.setState({ active: 'Objetivos' })}
        />
        <BottomNavigation.Action
            key="people"
            // icon="people"
            label="People"
            // onPress={() => this.setState({ active: 'people' })}
        />
        <BottomNavigation.Action
            key="bookmark-border"
            // icon="bookmark-border"
            label="Bookmark"
            // onPress={() => this.setState({ active: 'bookmark-border' })}
        />
        <BottomNavigation.Action
            key="settings"
            // icon="settings"
            label="Settings"
            // onPress={() => this.setState({ active: 'settings' })}
        />
      </BottomNavigation>
      </View>
    )
    // </View>
  }
  // return  <MainDrawer.Navigator initialRouteName="Objetivos">
  //           <MainDrawer.Screen name="Objetivos" component={Objetivos} />
  //         </MainDrawer.Navigator>
}


// render() {
//     <BottomNavigation active={this.state.active} hidden={false} >
//         <BottomNavigation.Action
//             key="today"
//             icon="today"
//             label="Today"
//             onPress={() => this.setState({ active: 'today' })}
//         />
//         <BottomNavigation.Action
//             key="people"
//             icon="people"
//             label="People"
//             onPress={() => this.setState({ active: 'people' })}
//         />
//         <BottomNavigation.Action
//             key="bookmark-border"
//             icon="bookmark-border"
//             label="Bookmark"
//             onPress={() => this.setState({ active: 'bookmark-border' })}
//         />
//         <BottomNavigation.Action
//             key="settings"
//             icon="settings"
//             label="Settings"
//             onPress={() => this.setState({ active: 'settings' })}
//         />
//     </BottomNavigation>
// }