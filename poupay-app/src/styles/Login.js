import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({

    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
  
    image: {
      flex: 1,
      resizeMode: "cover",
      justifyContent: "center"
    },
  
    logo: {
      width: 260,
      height: 150,
      marginTop: -70,
      marginBottom: 80,
      borderRadius: 10
    },
  
    input: {
      marginTop: 10,
      padding: 10,
      width: 300,
      fontSize: 16,
      fontWeight: 'bold',
      borderRadius: 3,
      borderBottomWidth: 1,
      color: '#000'
    },
  
    button: {
      width: 300,
      height: 60,
      backgroundColor: '#000000',
      marginTop: 20,
      borderRadius: 4,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 150
    },
  
    buttonFacebook: {
      width: 160,
      backgroundColor: '#3b5998',
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 150,
      marginHorizontal: 3
    },
  
    buttonGoogle: {
      width: 160,
      backgroundColor: '#FF0000',
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 150,
      marginHorizontal: 3
    },
  
    buttonText: {
      fontSize: 16,
      color: '#FFFFFF'
    },
  
    buttonTextRegister: {
      marginTop: 20,
      fontSize: 14,
      borderBottomColor: '#000',
      borderBottomWidth: StyleSheet.hairlineWidth,
      color: '#000'
    },
  
    text: {
      marginTop: 20,
      marginBottom: 10,
      fontSize: 16
    },
  
    fixToText: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      height: 55,
      marginTop: 20,
      marginBottom: 20
    },
  
})

export default styles;