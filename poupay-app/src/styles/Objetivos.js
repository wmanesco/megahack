import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },

    postContainer: {
        flexDirection: 'row',
        marginHorizontal: 10,
        marginVertical: 10,
        padding: 20,
        borderRadius: 3,
        borderRadius: 15,
        borderWidth: 2,
        borderColor: '#088A85',
        alignItems: 'center'
    },

    buttonRight: {
        justifyContent: 'center'
    },

    postTitle: {
        marginHorizontal: 20,
        fontSize: 18,
        fontWeight: "bold",
        marginBottom: 5
    },

    postDescription: {
        color: "#666"
    },

    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    },

    icon: {
        width:40,
        height:40
    },

    iconedit: {
        width:20,
        height:20
    },

    buttonEdit: {
        position: 'absolute',
        alignItems:'center',
        justifyContent:'center',
        padding: 15,
        width:1,
        right: 12
    }

});

export default styles;
