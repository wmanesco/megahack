import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        alignItems:'center',
        justifyContent:'center',
        padding: 25,
        backgroundColor: '#F78502',
        width:80,
        height:80,
        borderRadius:50
    },

    text: {
        color: '#FFFFFF',
        fontSize: 50
    }

})

export default styles;