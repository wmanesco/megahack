import api from './api';
import {AsyncStorage} from 'react-native';
import axios from 'axios'

export default class Users {
    
    constructor(){
        
    }

    async register(user, callback){
        
        axios.post("http://192.168.0.105:3000/users/register", user)
        .then(function (response) {
            if (response.ok) {
                callback(response.data, null);
            } else {
                callback(null, response.data.message)
            }            
          })
          .catch(function (error, a) {
            callback(null, JSON.stringify(error.response.data.message));
          });
    }

    authenticate(email, password, callback) {
        
        axios.post("http://192.168.0.105:3000/users/authenticate", {email: email, password: password})
        .then(function (response) {
            if (response.ok) {
                const { token } = response.data;
                AsyncStorage.multiSet([
                  ['@PoupayApp:token', token],
                  ['@PoupayApp:email', email],
                ]);

                callback(response.data, null);
            } else {
                callback(null, response.data.message)
            }            
          })
          .catch(function (error, a) {
            callback(null, JSON.stringify(error.response.data.message));
          });

    }
}